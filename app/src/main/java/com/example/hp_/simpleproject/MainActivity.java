package com.example.hp_.simpleproject;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener {
    //tabs
    private ActionBar actionBar;
    private String[] myTabs;

    // swipe
    private ViewPager viewPager;
    private StartTabsFragmentAdpter tabsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // to get action bar and set on it tabs
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        setContentView(R.layout.activity_main);
        myTabs = new String[]{"tab1", "tab2", "tab3", "tab4"};


        //swipe
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabsAdapter = new StartTabsFragmentAdpter(getSupportFragmentManager());
        viewPager.setAdapter(tabsAdapter);


        // set text on action bar
        for (int i=0;i<myTabs.length;i++) {
            actionBar.addTab(actionBar.newTab().setText(myTabs[i])
                    .setTabListener(this));
        }





        // swipe








        // change page with touch
       viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
           @Override
           public void onPageScrolled(int i, float v, int i2) {

           }

           @Override
           public void onPageSelected(int i) {
               actionBar.setSelectedNavigationItem(i);
           }

           @Override
           public void onPageScrollStateChanged(int i) {

           }
       });
        viewPager.setCurrentItem(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
